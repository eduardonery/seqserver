# Usando a imagem oficial do Seq
FROM datalust/seq:latest

# Expor as portas usadas pelo Seq
EXPOSE 80 5341

# Comando para iniciar o Seq
CMD ["seq"]
